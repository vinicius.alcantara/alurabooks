FROM nginx:latest
LABEL maintainer="vinícius alcântara"
WORKDIR /var/www/public
COPY ./public .
RUN chmod 775 -R /var/www/public
COPY ./docker/config/nginx.conf /etc/nginx
EXPOSE 80 443
CMD ["-g", "daemon off;"]
ENTRYPOINT ["nginx"]